// Greet component should render 'Hello' text 
// and also render the name given after 'Hello'



import { render, screen } from "@testing-library/react-native";
import Greet from "./greet";
describe('Greet', () => {
    test('Greet render correctly', () => {
    render(<Greet />);
    const connectedComponent = screen.getAllByText(/Hello/);
    expect(connectedComponent).toBeTruthy();
})

test('Greet render correctly name', () => {
    render(<Greet name='Saurabh' />);
    const testElement = screen.getByText(/Hello Saurabh/);
    expect(testElement).toBeTruthy()
})
})