import { Text } from "react-native"

interface GreetType{
    name?: string
}
const Greet = (props: GreetType) => {
    return <Text>Hello {props.name}</Text>
}
export default Greet;0