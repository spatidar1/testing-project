import React, {useRef, useState} from 'react';
import {Text, TextInput} from 'react-native';

function App() {
  const [Text, setText] = useState('');
  const ref = useRef();
  const [skip, setSkip] = useState(false);

  const onChangeText = (text: any) => {
    if (text.length < Text.length) {
      if (text.charAt(text.length - 2) === ' ') {
        setSkip(true);
      }
    } else {
      setSkip(false);
    }
    let data = text.split(' ').reduce((acc, curr) => acc + curr, "");
    if(data.length>= 5){
      data = data.substring(0, 4) + ' ' + data.substring(4);
    }
    if (data.length >= 8) {
      data = data.substring(0, 7) + ' ' + data.substring(7);
    }
    setText(data);
  };

  const handleSelectionChange = ({ nativeEvent: { selection: s } }) => {
    const select = s;
    if (skip) {
      const data = {
          start: select.start + 1,
          end: select.end + 1,
      };
      console.log('@@@',data);
      ref.setNativeProps({ selection: data });
    } else {
      console.log('!!!', s);
      // setSelection({ s });
    };
  }
  
  return <TextInput
    ref={ref}
    // selection={selection}
    style={{ marginTop: 200, width: 200, alignSelf: 'center', justifyContent: 'center', borderWidth: 2 }}
    value={Text}
    onChangeText={onChangeText}
    onSelectionChange={handleSelectionChange}
  />
}
export default App;
