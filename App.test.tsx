//qwk
import React from 'react';
import {render, screen} from '@testing-library/react-native';
import App from './App';

test('renders learn test ', () => {
  render(<App />); // create virtual dom for testing.
  const connectElement = screen.getByText(/learn react/); //screen is an object used to query virtual dom using its method.
  expect(connectElement).toBeTruthy();
});
